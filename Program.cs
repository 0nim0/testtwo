﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure; // Namespace for CloudConfigurationManager
// ******** Table is supported via Azure Cosmos, don't use these namespaces for Queue!!! ******** 
using Microsoft.Azure.Cosmos.Table;
using CloudTableClient = Microsoft.Azure.Cosmos.Table.CloudTableClient;
using CloudTable = Microsoft.Azure.Cosmos.Table.CloudTable;
using TableEntity = Microsoft.Azure.Cosmos.Table.TableEntity;
using TableQuery = Microsoft.Azure.Cosmos.Table.TableQuery;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Net;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;


namespace QueueTrigger
{
    class Program
    {
        // Global variables Google Drive
        static readonly string[] scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "Azure Marketplace Backup";
        static readonly string sheet_id = "1xNPrEswvoNrkqAxBjhmfA4HNvp-iF3QNQsLqfJkN4GU";
        static readonly string sheet_name = "Azure_Backup";
        static SheetsService sheet_service;


        static void Main(string[] args)
        {

            //Table code
            Microsoft.Azure.Cosmos.Table.CloudStorageAccount storageAccount = Microsoft.Azure.Cosmos.Table.CloudStorageAccount.Parse(
                       CloudConfigurationManager.GetSetting("StorageConnectionString"));

            //Creates a table client
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient();

            //Should create a CloudTable object representing our leads table
            CloudTable table = tableClient.GetTableReference("MarketplaceLeads");
            //table.CreateIfNotExistsAsync(); 

            //Queries for any non-empty entry
            var entities = table.ExecuteQuery(new Microsoft.Azure.Cosmos.Table.TableQuery<TableEntity>()).ToList();

            /*
            //Queue code
            CloudStorageAccount storageAccountQueue = CloudStorageAccount.Parse(
            CloudConfigurationManager.GetSetting("StorageConnectionString"));
            CloudQueueClient queueClient = storageAccountQueue.CreateCloudQueueClient();

            CloudQueue queue = queueClient.GetQueueReference("leadsqueue"); // Retrieve a reference to a container.
            queue.CreateIfNotExists();     // Create the queue if it doesn't already exist
            */
            var query = new TableQuery().Take(1000);
            var result = table.ExecuteQuery(query).ToList();
            int j = 0;
            while (j < result.Count && result != null)
            {
                if (result[j] != null && result.Count > 0)
                {
                    var dynamicTableEntity = result[j];
                    foreach (var property in dynamicTableEntity.Properties)
                    {
                        if (property.Key == "CustomerInfo")
                        {
                            parsend(property.Value.ToString(), null);
                        }
                    }

                    //Delete Entry from Azure Storage
                    TableOperation delete = TableOperation.Delete(result[j]);
                    table.Execute(delete);

                }
                j++;
            }  
        }


        
        public static bool Post_To_HubSpot_FormsAPI(int intPortalID, string strFormGUID, Dictionary<string, string> dictFormValues, string strHubSpotUTK, string strIpAddress, string strPageTitle, string strPageURL, ref string strMessage)
        {

            // Build Endpoint URL
            string strEndpointURL = string.Format("https://forms.hubspot.com/uploads/form/v2/{0}/{1}", intPortalID, strFormGUID);

            // Setup HubSpot Context Object
            Dictionary<string, string> hsContext = new Dictionary<string, string>();
            hsContext.Add("hutk", "60c2ccdfe4892f0fa0593940b12c11aa");
            hsContext.Add("ipAddress", "192.168.1.12");
            hsContext.Add("pageUrl", "http://demo.hubapi.com/contact/"); 
            hsContext.Add("pageName", "Microsoft Azure");
            
            // Serialize HubSpot Context to JSON (string)

            string strHubSpotContextJSON = JsonConvert.SerializeObject(hsContext);

            // Create string with post data
            string strPostData = "";

            // Add dictionary values
            foreach (var d in dictFormValues)
            {
                strPostData += d.Key + "=" + System.Web.HttpUtility.UrlEncode(d.Value) + "&";
            }

            // Append HS Context JSON
            strPostData += "hs_context=" + System.Web.HttpUtility.UrlEncode(strHubSpotContextJSON);

            // Create web request object
            System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(strEndpointURL);

            // Set headers for POST
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = strPostData.Length;
            request.KeepAlive = false;



            // *****************************************************

            // Uncomment this to send to Hubspot AP
            // There is some problem with this (This only sends 3 records (out of 5), need to debug and figure out why)

            // *****************************************************

            // POST data to endpoint

            System.IO.Stream strm = request.GetRequestStream();
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(strm))
            {
                sw.AutoFlush = true;
                try
                {
                    sw.Flush();
                    sw.Write(strPostData);
                    var response = request.GetResponse() as HttpWebResponse;
                    response.Dispose();
                }
                catch (Exception ex)
                {
                    // POST Request Failed
                    strMessage = ex.Message;
                    Console.WriteLine(strMessage);
                    sw.Flush();
 
                    return false;
                }
                sw.Close();     
            }
           
            return true; //POST Succeeded
        }
        
        public static void parsend(string sender, System.EventArgs e)
        {
            GoogleCredential creds;
            using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
            {
                creds = GoogleCredential.FromStream(stream).CreateScoped(scopes);
            }
            sheet_service = new SheetsService(new Google.Apis.Services.BaseClientService.Initializer()
            {
                HttpClientInitializer = creds,
                ApplicationName = ApplicationName,
            });


            Dictionary<string,string> dictFormValues = parse(sender);
                
            // Form Variables (from the HubSpot Form Edit Screen)
            int intPortalID = 1534169; //place your portal ID here
            string strFormGUID = "469f730b-8766-49d9-87ff-31686f781902"; //place your form guid here

            // Tracking Code Variables - Turned off
            // string strHubSpotUTK = Request.Cookies["hubspotutk"].Value;
            // string strIpAddress = System.Web.HttpContext.Current.Request.UserHostAddress;

            // Page Variables
            // string strPageTitle = "Microsoft Azure Partner Portal";
           // string strPageURL = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

            // Do the post, returns true/false
            string strError = "";
            bool blnRet = Post_To_HubSpot_FormsAPI(intPortalID, strFormGUID, dictFormValues, "", "", "", "", ref strError);
            if (blnRet)
            {
                Console.WriteLine("Post Successful");
                return;
            }
            else
            {
                Console.WriteLine("Post Failed");
                return;
            }
        }

        public static Dictionary<String,String> parse (string sender)
        {
            // *Example input and regex
            // {"FirstName":"Andrew","LastName":"Homer","Email":"andrew.homer@morphisec.com","Phone":"508-314-7632","Country":"USA","Company":"morphisec.com","Title":"Vice President, Business Development "}//

            Match txtFirstName = Regex.Match(sender, "(?<=\"FirstName\":\").*(?=\",\"LastName\":)");
            Match txtLastName = Regex.Match(sender, "(?<=\"LastName\":\").*(?=\",\"Email\":)");
            Match txtEmail = Regex.Match(sender, "(?<=\"Email\":\").*(?=\",\"Phone\":)");
            Match txtJob = Regex.Match(sender, "(?<=\"Title\":\").*(?=\")");
            Match txtCompany = Regex.Match(sender, "(?<=\"Company\":\").*(?=\",\"Title\":)");
            Match txtCountry = Regex.Match(sender, "(?<=\"Country\":\").*(?=\",\"Company\":)");
            Match txtPhone = Regex.Match(sender, "(?<=\"Phone\":\").*(?=\",\"Country\":)");

            // Build dictionary of field names/values (must match the HS field names)
            Dictionary<string, string> dictFormValues = new Dictionary<string, string>();
            dictFormValues.Add("firstname", txtFirstName.ToString());
            dictFormValues.Add("lastname", txtLastName.ToString());
            dictFormValues.Add("email", txtEmail.ToString());
            dictFormValues.Add("jobtitle", txtJob.ToString());
            dictFormValues.Add("company", txtCompany.ToString());
            dictFormValues.Add("country", txtCountry.ToString());
            dictFormValues.Add("phone", txtPhone.ToString());
                     
            List<object> toAdd = new List<object>() { txtFirstName.ToString(), txtLastName.ToString(), txtEmail.ToString(), txtJob.ToString(), txtCompany.ToString(), txtCountry.ToString(), txtPhone.ToString() };
            Backup_Table(toAdd);
            
            return dictFormValues;
        }

        public static void Backup_Table(List<object> toAdd)
        {
            var range = $"{sheet_name}!A:G";
            var valrange = new ValueRange();
            valrange.Values = new List<IList<object>> { toAdd }; //check
            var appendRequest = sheet_service.Spreadsheets.Values.Append(valrange, sheet_id, range);
            appendRequest.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            var appendResponse = appendRequest.Execute();
        }

    }
}
